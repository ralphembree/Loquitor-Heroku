# Loquitor-Heroku

This is an extension to https://github.com/ralphembree/Loquitor for running on Heroku.

## How to use

1. Fork this repository.
1. Edit the Procfile to make it enter the rooms you want.  Currently, you can't run on more than one chat host, but I'll probably fix that in the future.
1. Sign up for a Heroku account.
1. Install the heroku helper command.  Instructions are given [here](https://devcenter.heroku.com/articles/heroku-command-line#download-and-install), but a simple `sudo apt-get install heroku` worked for me on Linux Mint.
1. Create a Heroku app.  This can be done easily on the website by clicking the "New" slider on the top right of your [dashboard](https://dashboard.heroku.com) and selecting "Create new app".
1. Define the necessary environment variables.
  1. Go to the "Settings" tab of your app's page: https://dashboard.heroku.com/apps/YOUR_APP_NAME
  1. Click "Reveal Config Vars".
  1. Add the necessary variables. `LOQ_EMAIL` and `LOQ_PASSWORD` are required.  `BING_ID` and `BING_SECRET` can be used if you have credentials for the Bing translation API.  If you don't, that just means that you won't be able to use the `translate` command.  If you would like to run Loquitor on a different StackExchange site, you will need to define `LOQ_SITE`.  Note that most sites use stackexchange.com as the host.  The only sites that don't are stackoverflow.com and meta.stackexchange.com.
1. Connect the app to your new Github repository.  This is done by clicking the "Deploy" tab of 
1. Open a command prompt and type `heroku scale worker=1 -a YOUR_APP_NAME`.

Done!  Loquitor should now be running under the account you specified in the rooms you want.
