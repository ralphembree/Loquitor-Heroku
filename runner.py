#!/usr/bin/env python3

import http.server
from multiprocessing import Process
import os
import socketserver
import subprocess
import sys
import tempfile
import threading
import time
import traceback

from Loquitor import bot

def get_vars():
    return {
        'username': os.getenv('LOQ_EMAIL'),
        'password': os.getenv('LOQ_PASSWORD'),
        'config_dir': os.path.expanduser("~/.loquitor"),
        'host': os.getenv('LOQ_SITE', 'stackoverflow.com'),
        'no_input': True,
    }

def run_loquitor(ids, procs, vars=None):
    if vars is None:
        vars = get_vars()
    print("Loading...", end="\r")
    try:
        _, room, _ = bot.main(room=ids.pop(0), **vars)
        print("Loquitor successfully started in {}.".format(room.name))
    except:
        traceback.print_exc()
    if ids:
        proc = Process(target=run_loquitor, args=(ids, vars))
        proc.start()
        procs.append(proc)
    while True:
        try:
            time.sleep(5)
        except KeyboardInterrupt:
            break


PORT = int(os.getenv("PORT", 5000))

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer(("", PORT), Handler)

procs = []
Process(target=run_loquitor, args=(sys.argv[1:], procs)).start()

for proc in procs:
    proc.join()
#threading.Thread(target=httpd.serve_forever).run()
